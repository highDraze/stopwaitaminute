from setuptools import setup

setup(name='stopwaitaminute',
    version='0.0.0',
    packages=[
        'opencv',
    ],
    install_requires=[
        'numpy',
        'opencv-python',
        'pyzbar',
        'imutils',
    ],
    include_package_data=True
)
