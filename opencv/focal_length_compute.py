import configparser
import getpass
import math
import sys

import cv2
import pyzbar.pyzbar as pyzbar


def longest_side(polygon):
    l = len(polygon)
    return max([math.sqrt((polygon[i][0] - polygon[(i+1) % l][0])**2
               + (polygon[i][1] - polygon[(i+1) % l][1])**2)
               for i in range(l)])


if __name__ == '__main__':
    config = configparser.ConfigParser()
    config.optionxform = str
    config.read('../config.ini')

    # Read known real world sizes from config
    KNOWN_DISTANCE = float(config['OpenCV']['FocalInitDistance'])
    KNOWN_WIDTH = float(config['OpenCV']['FocalInitWidth'])

    # Load reference image
    image = cv2.imread(config['OpenCV']['FocalLengthInitPic'])

    # Locate the barcode
    barcodes = pyzbar.decode(image)
    if len(barcodes) != 1:
        print("Error: Please pick a picture with exactly one barcode!")
        sys.exit(1)
    perceived_width = longest_side(barcodes[0].polygon)

    # Calculate focal length using triangle similarity
    focal_length = (perceived_width * KNOWN_DISTANCE) / KNOWN_WIDTH

    # Write value into config
    key = 'FocalLength' + str(getpass.getuser())
    config['OpenCV'][key] = str(focal_length)
    with open('../config.ini', 'w') as configfile:
        config.write(configfile)
        configfile.close()
