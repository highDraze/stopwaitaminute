import time
import datetime
import getpass
import configparser
import math

import cv2
import imutils
import pyzbar.pyzbar as pyzbar
import numpy as np
from imutils.video import VideoStream

from opencv.focal_length_compute import longest_side

#keys = ["Blue", "Red"]  #jonas
keys = ["Bus", "Mensch"] #ari

class CapturedObject:
    name = ""
    location = tuple
    last_seen = None
    speed = None

    def __init__(self, name="", location=(0.0, 0.0)):
        self.name = name
        self.location = location
        self.speed = 0.0

    def available(self):
        if self.last_seen:
            delta = datetime.datetime.now() - self.last_seen
            return delta.total_seconds() < 1
        return False


NOBUS, BUSARRIVED, HUMANARRIVED, WAITING, BUSGONE = range(5)

def rootofevil(loc1, loc2):
    return math.sqrt((loc1[0] - loc2[0])**2 + (loc2[1] - loc1[1])**2)


def centroid(points):
    length = points.shape[0]
    sum_x = np.sum(points[:, 0])
    sum_y = np.sum(points[:, 1])
    return sum_x / length, sum_y / length


def decode(im):
    # Find barcodes and QR codes
    decoded_objects = pyzbar.decode(im)

    # Print results
    # for obj in decoded_objects:
    #  print('Type : ', obj.type)
    #  print('Data : ', obj.data, '\n')

    return decoded_objects


def distance_to_camera(known_width, focal_length, pwidth):
    if pwidth == 0:
        pwidth = 1
    return (known_width * focal_length) / pwidth


def locate_object(polygon, r, fov, w):
    """
    :param polygon: List of points
    :param r: Radius
    :param fov: Field of View
    :param w: Screen width in pixels
    :return: Cartesian x,y  in metres
    """
    # Use centroid of polygon as position
    x_pixel = centroid(np.array([[p[0], p[1]] for p in polygon]))[0]

    # Calculate angle from screen x point and fov
    angle = np.radians(fov * x_pixel / w - fov / 2)

    # Polar coordinates (angle, r) to cartesian (x, y)
    r /= 100
    cart = (r * np.sin(angle), r * np.cos(angle))
    return cart


def extract_info_from_frame():
    for barcode in decode(frame):
        global captured_code, busav, manav, distance_last, distance_now, tslot, thold
        # Extract the bounding box location of the barcode and draw
        # the bounding box surrounding the barcode on the image
        corners = [[p[0], p[1]] for p in barcode.polygon]
        corners = np.array([corners], dtype=np.int32)
        cv2.polylines(frame, corners, True, (0, 0, 255))
        #time and threshold adjustment
        thold = 0.03
        # The barcode data is a bytes object so if we want to draw it
        # on our output image we need to convert it to a string first
        barcode_data = barcode.data.decode("utf-8")

        # Calculate distance and coordinates
        distance = distance_to_camera(float(config['BarcodeWidth']),
                                      float(config['FocalLength' + getpass.getuser()]),
                                      longest_side(barcode.polygon))
        fov = float(config['FOV' + getpass.getuser()])
        coord = locate_object(barcode.polygon, distance, fov, width)

        display_capture_info(coord, corners, distance, frame, barcode_data)

        # If the barcode text is currently not in our CSV file, write
        # The timestamp + barcode to disk and update the set
        if barcode_data not in found:
            csv.write("{},{}\n".format(datetime.datetime.now(),
                                       barcode_data))
            csv.flush()
            found.add(barcode_data)

        # extract speed
        if not tslot or (datetime.datetime.now() - tslot).total_seconds() > 2:
            distance_last = rootofevil(captured_code[keys[0]].location, captured_code[keys[1]].location)

        now = datetime.datetime.now()
        delta_t = now - captured_code[barcode_data].last_seen if captured_code[barcode_data].last_seen else datetime.timedelta.max
        if not captured_code[barcode_data].last_seen or delta_t >= datetime.timedelta(0, 0, 500000):
            captured_code[barcode_data].last_seen = datetime.datetime.now()

            captured_code[barcode_data].speed = rootofevil(coord, captured_code[barcode_data].location)
            captured_code[barcode_data].speed /= delta_t.total_seconds()

            captured_code[barcode_data].location = coord

        adjust_state_machine(captured_code, distance_last, thold)


def adjust_state_machine(captured_code, distance_last, thold):
    global busav, manav, distance_now, tslot
    # Logic Distance
    # Trigger, Bus bewegt sich und Person bewegt sich in richtung buss
    # kein Trigger Bus bewegt sich und Person bewegt sich von von weiter weg zum sich entfernenden Bus
    busav = captured_code[keys[0]].available()
    manav = captured_code[keys[1]].available()
    distance_now = rootofevil(captured_code[keys[0]].location, captured_code[keys[1]].location)
    global state
    if state == NOBUS and busav:
        print("The bus has arrived")
        state = BUSARRIVED
    if state == BUSARRIVED and captured_code[keys[0]].speed != 0 and manav:
        print("Somebody is near the Bus Stop")
        state = HUMANARRIVED
    if state == HUMANARRIVED and captured_code[keys[1]].speed != 0:
        print("waiting")
        state = WAITING
    if state == WAITING and distance_now - distance_last > thold:
        print("The Bus is gone!")
        # print("DON'T GO!'")
        # print(round(captured_code[keys[1]].speed, 4))
        tslot = datetime.datetime.now()
        state = BUSGONE
    if state == BUSGONE:
        print("The taxi moves to client")
        state = -1


def display_capture_info(coord, corners, distance, frame, object_key):
    global captured_code
    ctext = "Coord: ({c[0]:.2f}m, {c[1]:.2f}m)".format(c=coord)
    dtext = "Distance: {:.2f}cm".format(distance)
    stext = "Speed: {:.2f}m/s".format(captured_code[object_key].speed)

    # Corners are three dimensional because of the fuck you from polylines before
    cv2.putText(frame, ctext, (corners[0, 2, 0], corners[0, 2, 1] - 15),
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
    cv2.putText(frame, dtext, (corners[0, 2, 0], corners[0, 2, 1] - 30),
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
    cv2.putText(frame, stext, (corners[0, 2, 0], corners[0, 2, 1] - 45),
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)


if __name__ == '__main__':
    config = configparser.ConfigParser()
    config.read('../config.ini')
    config = config['OpenCV']

    (width, height) = int(config['ScreenWidth']), int(config['ScreenHeight'])

    video_stream = VideoStream(src=0, resolution=(width, height)).start()
    time.sleep(2.0)

    csv = open(config['BarcodeOutput'], "w")
    found = set()
    tslot = None
    captured_code = {keys[0]: CapturedObject(name=keys[0]), keys[1]: CapturedObject(name=keys[1])}
    # Frame loop
    state = NOBUS
    while True:
        # Grab the frame from the threaded video stream and resize it to
        # have a maximum width of 400 pixels
        frame = video_stream.read()
        frame = imutils.resize(frame, width=400)

        # Find the barcodes in the frame and decode each of the barcodes
        # Loop over the detected barcodes
        extract_info_from_frame()

        # Show the output frame
        cv2.imshow("Barcode Scanner", frame)
        key = cv2.waitKey(1) & 0xFF

        # If the `q` key was pressed, break from the loop
        if key == ord("q"):
            break

    # Close the output CSV file and do a bit of cleanup
    print("[INFO] cleaning up...")
    csv.close()
    cv2.destroyAllWindows()
    video_stream.stop()
